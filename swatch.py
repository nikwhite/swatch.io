import flask
from flask import Flask
 
app = Flask(__name__)

app.debug = True

@app.route('/')
def index():
    return flask.render_template('index.html')

@app.route('/api/random')
def random():
    import random
    r = lambda: random.randint(0,255)
    dict = {'hex': '#%02X%02X%02X' % (r(),r(),r())}
    return flask.jsonify(**dict)

if __name__=="__main__":
    app.run()