module.exports = function(grunt) {
	
	grunt.initConfig({
		
		sass: {
			dev: {
				options: {
					style: 'nested',
					sourcemap: true
				},
				src: './static/sass/swatch.scss',
				dest: './static/css/swatch.css'
			},
			dist: {
				options: {
					style: 'compressed'
				},
				src: './static/sass/swatch.scss',
				dest: './static/css/swatch.min.css'
			}
		},

		handlebars: {
			compile: {
				options: {
					namespace: false,
					amd: true,
				},
				expand: true,
				src: 'static/js/view/*.hbs',
				dest: './',
				ext: '.js'
			}
		},
		
		requirejs: {
			compile: {
				options: {
				    baseUrl: "./static/js/",
				    mainConfigFile: "./static/js/main.js",
				    name: "main",
				    out: "./static/js/dist/swatch.min.js"
				}
			}
		},

		watch: {
			sass: {
				files: ['static/sass/*.scss', 'static/sass/partials/*.scss'],
				tasks: ['sass']
			},
			templates: {
				files: ['static/js/view/*.hbs'],
				tasks: ['handlebars']
			}
		}
	});
	
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-requirejs');
	grunt.loadNpmTasks('grunt-contrib-handlebars');
	grunt.loadNpmTasks('grunt-contrib-watch');
	
	grunt.registerTask('default', ['handlebars', 'requirejs', 'sass']);
	grunt.registerTask('dev', ['handlebars', 'sass:dev']);
};
