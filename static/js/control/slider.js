define(['mediator', 'util/function'], function(Mediator, funcUtils){

	var Slider = function(element, options){
		
		if (!element) throw 'Error: Slider requires an element';
		
		this.el = element;
		this.options = options ? options : {};
		
		this.changeCallback = funcUtils.debounce( this.onChange.bind(this), options.debounce || 0 );
		
		this.getInput().addEventListener('change', this.changeCallback);
		
		Mediator.subscribe( Mediator.CROSSHAIR_DRAG, this.disable.bind(this));
	};
	
	Slider.prototype.onChange = function(event){
		var val = this.getValue();
		
		this.displayValue(val);
		
		this.options.onChange && this.options.onChange(val);
	};
	
	Slider.prototype.getInput = function(){
		
		if ( !this.range ){
			this.range = this.el.querySelector( this.options.rangeSelector || 'input[type="range"]' );
		}
		
		return this.range;
	};
	
	Slider.prototype.getValue = function(){
		
		return this.getInput().value;
	};
	
	Slider.prototype.setValue = function(value){
		
		this.getInput().value = value;
		
		this.displayValue(value);
		
		return this;
	};
	
	Slider.prototype.getView = function(){
		
		if ( !this.view ){
			this.view = this.el.querySelector( this.options.viewSelector || '.value' );
		}
		
		return this.view;
	};
	
	Slider.prototype.displayValue = function(value){
		
		var view = this.getView();
		
		if (view) view.textContent = value;
		
		return this;
	};
	
	Slider.prototype.disable = function(bool){
		
		this.getInput().disabled = bool;
	};
	
	Slider.prototype.destroy = function(){
		
		this.getInput.removeEventListener('change', this.changeCallback);
	};
	
	
	
	return Slider;
});
