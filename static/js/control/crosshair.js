
define(['mediator'], function(Mediator){
	
	var Mediator;
	require(['mediator'], function(M){ Mediator = M; });
	
	var Crosshair = function(element, canvas){
		var self = this;
		
		this.el = element;
		this.position = { top: 0, left: 0 };
		
		if ( !canvas ){
			throw 'Crosshairs must be provided a reference object which mixes Canvas';
		} else {
			this.canvas = canvas;
		}
		
		this.bounds = this.canvas.getCanvasBounds();
		
		this.el.addEventListener('mousedown', this.start.bind(this));
		
		Mediator.subscribe( Mediator.MOUSE_LEFT_WINDOW, this.end.bind(this) );
	}
	
	
	Crosshair.prototype.setPosition = function( x, y ){
		
		this.position.left = x;
		this.position.top = y;
		
		this.el.style.left = x - 5 + 'px';
		this.el.style.top = y - 5 + 'px';
	}
	
	
	Crosshair.prototype.setDisplay = function(val){
		this.el.style.display = val;
	}
	
	
	Crosshair.prototype.start = function(event){
		
		// Set bounds when the drag starts
		this.bounds = this.canvas.getCanvasBounds();
		this.scrollTop = document.body.scrollTop;
		
		// Cache bindings to remove later
		this.boundMove = this.move.bind(this);
		this.boundEnd = this.end.bind(this);
		
		window.addEventListener('mousemove', this.boundMove);
		window.addEventListener('mouseup', this.boundEnd);
		
		Mediator.publish( Mediator.CROSSHAIR_DRAG, true );
	}
	
	
	Crosshair.prototype.move = function(event, typeClick){
		
		var fakeEvent = { pageX: event.pageX, pageY: event.pageY }
		
		if ( typeClick ) fakeEvent.type = 'click';
		
		if ( event.pageY < this.bounds.top + this.scrollTop ) {
			fakeEvent.pageY = this.bounds.top + this.scrollTop;
			
		} else if ( event.pageY >= this.bounds.bottom + this.scrollTop ) {
			fakeEvent.pageY = this.bounds.bottom - 1 + this.scrollTop;	
		}
		
		if ( event.pageX < this.bounds.left ){
			fakeEvent.pageX = this.bounds.left;
			
		} else if ( event.pageX > this.bounds.right ){
			fakeEvent.pageX = this.bounds.right - 1;
		}
		
		this.canvas.changeColor(fakeEvent);
	}
	
	
	Crosshair.prototype.end = function(event){
		
		event.type && this.move(event, true);
		
		window.removeEventListener('mousemove', this.boundMove);
		window.removeEventListener('mouseup', this.boundEnd);
		
		Mediator.publish( Mediator.CROSSHAIR_DRAG, false );
	}
	
	return Crosshair;
});
