define( ['mixture', 'mixin/canvas', 'model/color', 'util/color'], function(mix, Canvas, Color, colorUtils) {

	var white = new Color([255,255,255]);
	
    var Wheel = function(options) {

// Private members ---------------------------------------------
		var radius = 150;
		var currentColors = [ ];
		
		function draw() {
			
			var center = radius -1;
			var r = radius;
	        var ctx = this.getCanvasContext( );
	        var increment = 0.25;
	        var divisions = currentColors.length;
	        var divisionDegrees = 360/divisions;
	        var i, l, d, angle, x1, y1, color, nextColor;
	        
	        // starting at the outside moving inward
	        while ( r-- ){
	        	
	        	// divide ring operation into d pieces
	        	for ( d = 0; d < divisions; d++){
	        		
	        		currentColor = currentColors[d].rgb;
	        		nextColor = currentColors[ d+1 >= divisions ? 0 : d+1 ].rgb;
	        		
	        		// draw pixels as a gradient between division edges
	        		for ( i = 0; i < divisionDegrees; i += increment){
	        			
			        	angle = i + divisionDegrees * d;
			        	x1 = r * Math.cos(angle * Math.PI / 180);
			        	y1 = r * Math.sin(angle * Math.PI / 180);
			        	
			        	this.putPixelRGB( 
			        		ctx, 
			        		center + x1, center + y1, 
			        		2, 2,
			        		colorUtils.getGradedRgb(currentColor, nextColor, i / divisionDegrees) 
		        		);
			        
			        }	
	        	}
	        }
		}
		draw = draw.bind(this);
		
		// Public members ---------------------------------------------
		this.pushColor = function(color, index) {
			
	    	if ( typeof index == 'number' ){
	    		currentColors[index] = color;
	    	} else {
	    		currentColors.push(color);
	    	}
	    	
    		draw( );
		}
		this.removeColor = function(color) {
			var i = currentColors.indexOf(color);
			
			if ( i > -1 ){
				
				currentColors.splice(i, 1);
				
				if ( currentColors.length === 0 ){
					this.reset();
				} else {
					draw( );
				}
			}
		}
		this.reset = function() {
			// draw a white circle
			currentColors = [ white ];
			draw( );
			// reset so white doesnt get mixed in with next color addition
			currentColors = [ ];
		}

        // Initialization ---------------------------------------------
        mix( Canvas ).using( options ).into( this );

        this.reset();
        
        currentColors = []; 
    }

    return Wheel;

} );
