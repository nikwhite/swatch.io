define(['model/color'], function(Color){
	
	var Dropzone = function(options){
		
		var element = options.dropElement;
		var callback = options.dropCallback;
		
		element.addEventListener('dragover', function(event){
			if ( event.dataTransfer.types.indexOf('application/x-color') !== -1 ){
				event.preventDefault();
			}
		});
	
		element.addEventListener('drop', function(event){
			if ( event.dataTransfer.types.indexOf('application/x-color') !== -1 ){
				callback(  
					JSON.parse( 
						event.dataTransfer.getData('application/x-color') 
					)
				);
			}
		});
	}
	
	return Dropzone;
	
});
