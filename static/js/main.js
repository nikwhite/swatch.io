require.config({
	paths: {
		'handlebars': 'vendor/handlebars',
		'$': 'vendor/zepto'
	}
});

require([
    'mediator', 
    'component/swatch', 
    'component/colorpicker', 
    'component/blender-palette', 
    'component/blender-wheel', 
    'model/color', 
    'component/swatch-group',
    'component/switcher'
], 

function( Mediator, Swatch, ColorPicker, Palette, Wheel, Color, SwatchGroup, Switcher ){
	
	var swatches = [];
    var swatchElements = Array.prototype.slice.call( document.querySelectorAll( '.swatch' ) );
    var colors = [new Color([123, 24, 76]), 
        	new Color([76, 24, 123]), 
        	new Color([24, 123, 76])]
	
	var swatchGrouping = new SwatchGroup({
		swatchColors: colors,
		swatchGroupId: 'swatchGroup'
	});

    var colorPicker = new ColorPicker( document.getElementById('colorPicker'), {
    	luminanceSelector: '#luminanceSlider'
    });
    
    var addButton = document.getElementById('addSwatch');
    
    addButton.addEventListener('click', function(e){
    	event.preventDefault();
    	swatchGrouping.addSwatch( new Color([255,255,255]) );
    });

    var toolNav = new Switcher({
        toggleSelector: '.tool-nav a',
        toggleGroupId: 'toolNav',
        contentSelector: '.tool'
    })
    
    var blender = new Palette( document.getElementById('blender') );
    
    var triWheel = new Wheel( document.getElementById('triWheel'), document.getElementById('wheel-dropzone') );
	
});
