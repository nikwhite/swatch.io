
define([], function(){
		
	var colorUtils = {}
	
	/**
	 * Converts an HSL color value to RGB. Conversion formula
	 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
	 * Assumes h, s, and l are contained in the set [0, 1] and
	 * returns r, g, and b in the set [0, 255].
	 *
	 * @param   Number  h       The hue
	 * @param   Number  s       The saturation
	 * @param   Number  l       The lightness
	 * @return  Array           The RGB representation
	 */
	colorUtils.hslToRgb = function (h, s, l) {
	    var r, g, b;

	    function hue2rgb(p, q, t){
            if(t < 0) t += 1;
            if(t > 1) t -= 1;
            if(t < 1/6) return p + (q - p) * 6 * t;
            if(t < 1/2) return q;
            if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
            return p;
        }

	    if (s == 0) {
	        r = g = b = l * 0.01; // achromatic

	    } else {

	    	h /= 360;
	    	s /= 100;
	    	l /= 100;

	        var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
	        var p = 2 * l - q;
	        r = hue2rgb(p, q, h + 1/3);
	        g = hue2rgb(p, q, h);
	        b = hue2rgb(p, q, h - 1/3);
	    }

	    return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
	}

	/**
	 * Converts an RGB color value to HSL. Conversion formula
	 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
	 * Assumes r, g, and b are contained in the set [0, 255] and
	 * returns h, s, and l in the set [0, 1].
	 *
	 * @param   {Number}  r       The red color value
	 * @param   {Number}  g       The green color value
	 * @param   {Number}  b       The blue color value
	 * @return  {Array}           The HSL representation
	 */
	colorUtils.rgbToHsl = function (r, g, b) {
	    r /= 255, g /= 255, b /= 255;
	    var max = Math.max(r, g, b), min = Math.min(r, g, b);
	    var h, s, l = (max + min) / 2;
	
	    if(max == min){
	        h = s = 0; // achromatic
	    }else{
	        var d = max - min;
	        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
	        switch(max){
	            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
	            case g: h = (b - r) / d + 2; break;
	            case b: h = (r - g) / d + 4; break;
	        }
	        h /= 6;
	    }
	
	    return [Math.round(h*360), Math.round(s*100), Math.round(l*100)];
	}
	
	/**
	 * Converts an RGB color value to Hexadecimal.
	 * Assumes r, g, and b are contained in the set [0, 255] and
	 * returns a hexadecimal string in the set [00, FF].
	 *
	 * @param   {Number}  r       The red color value
	 * @param   {Number}  g       The green color value
	 * @param   {Number}  b       The blue color value
	 * @return  {String}          The hexadecimal representation
	 */
	colorUtils.rgbToHex = function(r, g, b) {
	    if (r > 255 || g > 255 || b > 255)
	        throw "Invalid color component";
	    return ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
	}
	
	/**
	 * Linear interpolation of 2 rgb colors at a specified percentage 
	 * @param {Array} rgb1 Array containing [r,g,b]
	 * @param {Array} rgb2 Array containing [r,g,b]
	 * @param {Number} percent in the set [0,1] (decimal notation)
	 * @return {Array} rgb [r,g,b] Array 
	 */
	colorUtils.getGradedRgb = function (rgb1, rgb2, percent) {
	    return [ 
	    	rgb1[0] + Math.floor( percent * (rgb2[0] - rgb1[0]) ),
	    	rgb1[1] + Math.floor( percent * (rgb2[1] - rgb1[1]) ),
	    	rgb1[2] + Math.floor( percent * (rgb2[2] - rgb1[2]) ),
	    ]
	}
	
	return colorUtils;
})
