
define([], function(){
	
	var funcUtils = {};
	
	// borrowed from mout/function/debounce
	funcUtils.debounce = function(fn, threshold, isAsap){
	    var timeout, result;
	    function debounced(){
	        var args = arguments, context = this;
	        function delayed(){
	            if (! isAsap) {
	                result = fn.apply(context, args);
	            }
	            timeout = null;
	        }
	        if (timeout) {
	            clearTimeout(timeout);
	        } else if (isAsap) {
	            result = fn.apply(context, args);
	        }
	        timeout = setTimeout(delayed, threshold);
	        return result;
	    }
	    return debounced;
	}
	
	return funcUtils;
});
