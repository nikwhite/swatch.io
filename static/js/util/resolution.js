define(['util/function', 'mediator'], function (funcUtils, mediator) {

	function resolutionChange(event) {
		mediator.publish('resolutionChange', event);
	}

	var dbResChange = funcUtils.debounce(resolutionChange);

	window.addEventListener('resize', dbResChange);
});