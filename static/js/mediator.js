define( [], function() {

    var HANGUP_MESSAGE = 'hangup';
    var COLOR_CHANGE = 'colorChange';
    var MOUSE_LEFT_WINDOW = 'mouseLeftWindow';
    var CROSSHAIR_DRAG = 'crosshairDrag';

/**************************************
 * Global events                      *
 **************************************/

	document.addEventListener('mouseout', function(e){
		e = e ? e : window.event;
		if ( !e.relatedTarget && !e.toElement ) {
			publish(MOUSE_LEFT_WINDOW, []);
		}
	});

	
/**************************************
 * PUB/SUB                            *
 **************************************/

    var topics = {};
    var subUid = -1;

    function subscribe(topic, func) {
        if ( !topics[topic] ) {
            topics[topic] = [];
        }
        var token = ( ++subUid ).toString( );
        topics[topic].push( {
            token: token,
            func: func
        } );
        return token;
    }
	
	// Publish with no setTimeout - instant messaging
    function publish(topic, args) {
        if ( !topics[topic] ) {
            return false;
        }

        var subscribers = topics[topic], len = subscribers ? subscribers.length : 0;
		
        while ( len-- ) {
            subscribers[len].func( args );
        }

        return true;

    };

    function unsubscribe(token) {
        for ( var m in topics ) {
            if ( topics.hasOwnProperty( m ) ) {
                for ( var i = 0, j = topics[m].length; i < j; i++ ) {
                    if ( topics[m][i].token === token ) {
                        topics[m].splice( i, 1 );
                        return token;
                    }
                }
            }
        }
        return false;
    }
    
    // Expose API
    return {
		
		HANGUP_MESSAGE:		HANGUP_MESSAGE,
        COLOR_CHANGE: 		COLOR_CHANGE,
        CROSSHAIR_DRAG:     CROSSHAIR_DRAG,
        MOUSE_LEFT_WINDOW: 	MOUSE_LEFT_WINDOW,

        publish: 			publish,
        subscribe: 			subscribe,
        unsubscribe: 		unsubscribe
    }

} );
