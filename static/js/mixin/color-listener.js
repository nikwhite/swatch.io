define( ['mediator'], function(Mediator) {
	
	/**
	 * ColorListener
 	 * @param {Object} options Implements options.colorListenerCallback
	 */
	function ColorListener(options){
		
		var subtoken = Mediator.subscribe( Mediator.COLOR_CHANGE, function(message){
			options.colorListenerCallback(message);
		} );
		
		this.unsubscribeFromColorChange = function () {
			Mediator.unsubscribe(subtoken);
			this.unsubscribeFromColorChange = undefined;
		}
	}

	return ColorListener;
} );
