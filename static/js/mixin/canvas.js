
define(['model/color'], function(Color){

	var Canvas = function(options) {

// Private members ---------------------------------------------		
		var canvas = options.canvasElement;

// Public members ---------------------------------------------
		this.getCanvasBounds = function () {
			
			return this.getCanvas().getBoundingClientRect();
		}
		
		this.getCanvasColor = function ( x, y ) {
			
			var rgb = this.getCanvasContext().getImageData( x, y, 1, 1 ).data;
			
			return new Color( [ rgb[0], rgb[1], rgb[2] ], x, y);
		}
		
		this.getCanvasContext = function () {
			
			return this.getCanvas().getContext('2d');
		}
		
		this.getCanvas = function () {
			
			return canvas;
		}
		
		this.clearCanvas = function () {
		
			var bounds = this.getCanvasBounds();
			var ctx = this.getCanvasContext();
			
			ctx.fillStyle = '#ffffff';
			ctx.fillRect( 0, 0, bounds.width, bounds.height );
			
			return this;
		}
		
		this.putPixelRGB = function (ctx, x, y, w, h, rgb) {
			
	    	ctx.fillStyle = 'rgb(' + rgb.join(',') + ')';
	    	ctx.fillRect( x, y, w, h); 
		}
		
	};
	
	return Canvas;
});

