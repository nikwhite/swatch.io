define(function(){
	
	function Activable(options) {
		
		this.setActive = function(x) {
			if ( typeof active !== undefined && mutuallyExclusive ) 
				this.unsetActive(active);
				
			if ( !mutuallyExclusive ) {
				active.push( x );
			} else {
				active = x 
			}	
			
			return element;
		}
		
		this.unsetActive = function(x) {
			if ( x ) {
				active.splice( active.indexOf(x), 1 );
			} else {
				active = undefined;
			}
		}
		
		this.isActive = function(x) {
			if ( mutuallyExclusive ) {
				return active === x;
			} else {
				return active.indexOf(x) > -1 ? false : true;
			}
		}

		var active = undefined;
		var mutuallyExclusive = true;
						
		if ( !options.mutuallyExclusive ) {
			mutuallyExclusive = false;
			active = [];
		}
	}
	
	return Activable;
});
