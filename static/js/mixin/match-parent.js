define( [
	'mixture',
	'mediator', 
	'vendor/event-emitter', 
	'util/resolution'
	], 
function (mix, mediator, EventEmitter) {

	function MatchParent(options) {

		var parent = options.parentToMatch;
		var child = options.childToMatch;
		var matchWidth = options.matchWidth || true;
		var matchHeight = options.matchHeight || true;
		var parentRect = {};
		var parentWidth = 0;
		var parentHeight = 0;

		function getParentDimensions() {
			parentRect = parent.getBoundingClientRect();
			parentWidth = parentRect.width;
			parentHeight = parentRect.height;
		}		

		function handleChange() {
			getParentDimensions();
			applyDimensionsToChild();
			this.emitEvent('dimensionChange');
		}

		function applyDimensionsToChild() {
			if (child.width) {
				child.width = parentWidth;
				child.height = parentHeight;
			} else {
				child.style.width = parentWidth + 'px';
				child.style.height = parentHeight + 'px';
			}
		}

		mediator.subscribe('resolutionChange', handleChange.bind(this));

		if ( !this.emitEvent ) {
			mix( EventEmitter ).into( this );
		}

		handleChange.call(this);
 
	}

	return MatchParent;


});