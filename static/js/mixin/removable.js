define(function(){
	
	/**
	 * 
	 * @param {Object} options Implements options.removableElement
	 */
	var Removable = function(options) {
		
		function removeHandler(event) {
			event.preventDefault();
			var $remove = $(event.target).closest(removeEl);
			var index = $remove.index();
			$remove.remove();
			removeCallback && removeCallback(index);
		}
		
		var element = options.removableDelegate;
		var triggerSelector = options.removableTriggerSelector || 'a.remove';
		var removeEl = options.removableElementSelector || 'li';
		var removeCallback = options.removeCallback;
		
		$(element).on('click', triggerSelector, removeHandler.bind(this));
		
	}
	
	return Removable;
});
