define(['$', 'handlebars'], function($, Handlebars) {
	
	Handlebars.registerHelper('toString', function(arr){
		return arr.join(', ');
	});
	

	/**
	 * Module to initiate views
 	 * @param {Object} options Implements options.view and options.viewElement
	 */
	var View = function(options){
		var view = options.view;
		var $renderTarget = $(options.renderTarget) || $(document.body);
		var renderMethod = options.renderMethod || 'append';
		var $rendition;
		
		function stringTo$html(str) {
			var div = document.createElement('div');
			div.innerHTML = str;
			return $(div).children();
		}
		
		this.renderView = function(data){
			var $html = stringTo$html( view(data) );
			$renderTarget[renderMethod]( $html );
			return $rendition = $html;
			
		}
		this.updateView = function (data) {
			var $html = stringTo$html( view(data) );
			$rendition.replaceWith( $html );
			return $rendition = $html; 
		}
		this.removeView = function () {
			return $rendition.remove();
		}	
		this.getView = function () {
			return $rendition;
		}	
	}
	
	return View;
});
