
define(['util/color'], function(colorUtils){
	
	/**
	 * Color - constructs a Color object for storing color and coordinate data
	 * @constructor
	 * @param {Array} rgb - Red, green and blue channels in [r,g,b] format
	 */
	var Color = function (rgb) { 
		var r = rgb[0];
		var g = rgb[1];
		var b = rgb[2];
		
		this.rgb = rgb;
		this.hex = colorUtils.rgbToHex( r, g, b );
		this.hsl = colorUtils.rgbToHsl( r, g, b );
	}
	
	Color.prototype.toJSON = function () {
		return {
			rgb: this.rgb,
			hsl: this.hsl,
			hex: this.hex
		}
	}

	Color.prototype.modify = function (colorSpace, index, value) {
		
		this[colorSpace][index] = value;

		if ( colorSpace.toLowerCase() === 'hsl' ) {
			this.rgb = colorUtils.hslToRgb.apply(this, this.hsl);

		} else if ( colorSpace.toLowerCase() === 'rgb' ) {
			this.hsl = colorUtils.rgbToHsl.apply(this, this.rgb);
		}

		this.hex = colorUtils.rgbToHex.apply(this, this.rgb);
	}
	
	return Color;
});
