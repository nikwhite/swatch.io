define(['mixture', 'mediator', 'mixin/canvas', 'mixin/color-listener', 'control/slider'], function(mix, Mediator, Canvas, ColorListener, Slider){

	var Blender = function(element) {
		
// Private members ---------------------------------------------
		var currentColor;
		var radius = 50;
		var hardness = 50;
		var ctx;		
		
		function drawBrush(x, y) {
			
			if ( !currentColor ) return;
		
			var rgb = currentColor.rgb.join(',');
			//var gradient = ctx.createRadialGradient( x, y, 0, x, y, radius );
			
			//gradient.addColorStop( hardness/101, 'rgba(' + rgb + ', 1)' );
			//gradient.addColorStop( 1, 'rgba(' + rgb + ', 0)' );
			
			ctx.fillStyle = '#' + currentColor.hex;
			ctx.fillRect(x,y,1,1);

		}
		drawBrush = drawBrush.bind(this);
		
		function start(event) {
			drawBrush( event.layerX, event.layerY );

			var canvas = this.getCanvas();
			canvas.addEventListener('mousemove', move.bind(this));
		}

		function move(event) {
			drawBrush( event.layerX, event.layerY );
		}

// Initialization ---------------------------------------------
		var brushSlider = new Slider( document.getElementById('brushSlider'), {
			onChange: function ( ) {
				radius = brushSlider.getValue();
			}
		} );
		
		var hardnessSlider = new Slider( document.getElementById('hardnessSlider'), {
			onChange: function ( ) {
				hardness = hardnessSlider.getValue();
			}
		} );
		
		var subToken = Mediator.subscribe( Mediator.COLOR_CHANGE, function(message) {
			if ( message === Mediator.HANGUP_MESSAGE ) return;
			currentColor = message;
		} );
		
		// Mix canvas utilites
		mix( Canvas, ColorListener ).using( {
			canvasElement: element,
			colorListenerCallback: function(message) {
				currentColor = message;
			}
		} ).into( this );

		ctx = this.getCanvasContext();
		this.getCanvas().addEventListener('mousedown', start.bind(this));
		
		this.clearCanvas();
	}
	
	return Blender;
});