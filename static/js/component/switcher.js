define([
	'mixture',
	'mixin/toggle',
	'control/toggle-group'
], 
function (mix, Toggle, ToggleGroup) {
	
	var Switcher = function (options) {

		var toggles = [];
		var content = [];
		var domToggles = Array.prototype.slice.call( document.querySelectorAll(options.toggleSelector) );
		var domContent = Array.prototype.slice.call( document.querySelectorAll(options.contentSelector) );

		domToggles.forEach( function (el) {
			toggles.push( 
				new Toggle({ 
					toggleElement: el
				})
			);
		});

		domContent.forEach( function (el) {
			content.push( 
				new Toggle({ 
					toggleElement: el
				})
			);
		});

		mix( ToggleGroup ).using({
			toggles: toggles,
			toggleGroupId: options.toggleGroupId,
			toggleSelector: options.toggleSelector,
			mutuallyExclusive: options.mutuallyExclusive || true
		}).into( this );

		this.addListener('toggle', function(toggle){
			// content behavior depends on tab active state and mirrors it
			matchingContent = content[ toggles.indexOf(toggle) ];
			matchingContent && matchingContent.toggle( toggle.isActive() );
		});
	};

	return Switcher;

});