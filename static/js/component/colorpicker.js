define( [
    'mixture', 
    'mediator', 
    'control/crosshair', 
    'control/slider', 
    'mixin/canvas', 
    'mixin/color-listener',
    'mixin/match-parent', 
    'util/function'
    ], 

function(mix, Mediator, Crosshair, Slider, Canvas, ColorListener, MatchParent, funcUtils) {

    /**
     * ColorPicker = constructs a ColorPicker object to interface with a <canvas> and Swatches
     * @mixes Canvas
     */
    var ColorPicker = function(element, options) {
       
// Private members ---------------------------------------------
		var currentColor;
        var colorHistory = [];
        
        /**
         * draw - draws the ColorPicker in hsl
         */
        function draw() {

            var h = 0;
            var s = 0;
            var luma = currentColor ? currentColor.hsl[2] : 50;
            var ctx = this.getCanvasContext( );
            var scaleX = element.width / 360;
            var scaleY = element.height / 100;
            var pxWidth = scaleX * 1.5;
            var pxHeight = scaleY * 1.5;

            while ( h <= 360 ) {

                s = 0;

                while ( s <= 100 ) {

                    ctx.fillStyle = 'hsl(' + h + ',' + s + '%,' + luma + '%)';
                    ctx.fillRect( h * scaleX, s * scaleY, pxWidth, pxHeight );
                    
                    s++;
                }

                h++;
            }
        }

        draw = draw.bind(this);

// Public members ---------------------------------------------
		/**
         * changeColor - handles events driven from changes in the color palette
         * @param {Event} event
         */
        this.changeColor = function changeColor(event) {

            var canvasX = event.pageX - this.getCanvasBounds( ).left;
            var canvasY = event.pageY - this.getCanvasBounds( ).top - document.body.scrollTop;

            var color = this.getCanvasColor( canvasX, canvasY );

            if ( event.type === 'click' || event.type === 'mouseup' ) {
                colorHistory.push( color );
            }

            Mediator.publish( Mediator.COLOR_CHANGE, color );
        };
        
        this.getColor = function getColor() {

            return currentColor;
        };

        this.setColor = function setColor(color) {

            var currentLuma = currentColor ? currentColor.hsl[2] : 50;
            var canvas = this.getCanvas();

            currentColor = color;

            crosshair.setPosition( color.hsl[0] * (canvas.width / 360), color.hsl[1] * (canvas.height / 100) );

            if ( currentLuma !== color.hsl[2] ){
                draw( );
            }
        };

// Initialization --------------------------------------------

 		// Mix canvas utilites
		mix( Canvas, ColorListener, MatchParent ).using( {
			canvasElement: element,
			colorListenerCallback: this.setColor.bind(this),
            parentToMatch: element.parentNode,
            childToMatch: element
		} ).into( this );

        var crosshair = new Crosshair(document.getElementById( 'crosshair' ), this);

        // Attach events
        this.getCanvas( ).addEventListener( 'click', this.changeColor.bind( this ) );
        

        this.addListener('dimensionChange', draw);
        
        draw( );

    }

    return ColorPicker;
} ); 