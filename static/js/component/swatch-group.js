define([
	'$', 
	'mediator', 
	'mixture', 
	'component/swatch', 
	'control/toggle-group', 
	'mixin/toggle', 
	'mixin/removable'
], 

function($, Mediator, mix, Swatch, ToggleGroup, Toggle, Removable){

	// using a canvas as a drag image doesnt seem to work unless its rendered in window
	var dragCanvas = document.createElementNS("http://www.w3.org/1999/xhtml","canvas");
	
	dragCanvas.id = 'dragCanvas';
	dragCanvas.width = dragCanvas.height = 20;
	dragCanvas.style.position = 'fixed';
	dragCanvas.style.zIndex = '-1';
	
	document.body.appendChild(dragCanvas);
	
	/**
	 * Creates a new SwatchGroup
	 * @class
	 * @param {Object} options
	 * @param {string} options.swatchGroupId
	 * @param {Color[]} options.swatchColors
	**/
	var SwatchGroup = function(options){
		
		var element = document.getElementById( options.swatchGroupId );
		var colors = options.swatchColors;
		var $element = $(element);
		var swatches = [ ];
				
		function dragStart(event) {
			var transfer = event.dataTransfer;
			var ctx = dragCanvas.getContext('2d');
			var color = swatches[ $(event.target).closest('li').index() ].getColor();
			
			ctx.clearRect(0,0,20,20);
			ctx.fillStyle = '#' + color.hex;
			ctx.fillRect(0,0,20,20);
			
			transfer.setDragImage(dragCanvas, 10, 10);
			transfer.setData('application/x-color', JSON.stringify( color.toJSON() ) );
			transfer.effectAllowed = 'copy';
		}
		
		function colorChange(message) {
			var activeToggle = this.getActiveToggle();

			if (activeToggle) {
				activeToggle.setColor(message);
			}
		}

		function colorAdjust(newColor) {
			Mediator.publish( Mediator.COLOR_CHANGE, newColor);
		}
		
	    function removeSwatch(index) {
	    	swatches.splice( index, 1 );
	    }
	    
	    /** 
	     * 
	     * @public
	    **/
	    this.addSwatch = function addSwatch(color) {
	    	var li = document.createElement('li');
	    	var swatch = mix( Swatch, Toggle ).using({
	    		color: color,
	    		draggable: true,
	    		renderTarget: li,
	    		renderMethod: 'append',
	    		toggleElement: li
	    	}).into({});
	    	
	    	element.appendChild(li);
	    	
	    	swatches.push( swatch );
	    	
	    	this.addToggle( swatch );
	    }
	    
	    mix( ToggleGroup, Removable ).using({
			toggles: [ ],
			mutuallyExclusive: true,
			toggleGroupId: options.swatchGroupId,
			toggleSelector: 'li',
			removableDelegate: $element,
			removableElement: 'li',
    		removeCallback: removeSwatch
		}).into( this );
		
	    colors.forEach( function(color){
	    	this.addSwatch(color);
	    }.bind( this ) );
	    
	    $element.on('dragstart', '.preview', dragStart.bind(this));

	    this.addListener('toggle', function(toggle){
	    	if ( toggle.isActive() ) {
	    		Mediator.publish( Mediator.COLOR_CHANGE, toggle.getColor() );
	    		toggle.addListener(Mediator.COLOR_CHANGE, colorAdjust);
	    	} else {
	    		toggle.removeListener(Mediator.COLOR_CHANGE, colorAdjust);
	    	}
	    });
		
		Mediator.subscribe( Mediator.COLOR_CHANGE, colorChange.bind(this));
	}
	
	return SwatchGroup;
});
