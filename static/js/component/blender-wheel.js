define(['mixture', 'control/wheel', 'control/dropzone', 'model/color', 'component/swatch'], 
function(mix, Wheel, Dropzone, Color, Swatch){
	
	var BlenderWheel = function(canvasEl, dropzoneEl){
		
		function onDrop(data) {
			var color = new Color(data.rgb);
			var swatch = new Swatch({
				color: color,
				renderTarget: dropzoneEl,
				draggable: false
			});
			
			setTimeout( function() { this.pushColor( color ); }.bind(this), 1 );
			
			swatch.getView().on('click.remove', 'a.remove', function(event){
				event.preventDefault();
				swatch.getView().off('click.remove');
				swatch.removeView();
				this.removeColor(color);
			}.bind(this));
		};
		
		mix( Wheel, Dropzone ).using( {
			
			canvasElement: canvasEl,
			dropElement: dropzoneEl,
			dropCallback: onDrop.bind(this)
			
		} ).into( this );
		

	}
	
	return BlenderWheel;
});
