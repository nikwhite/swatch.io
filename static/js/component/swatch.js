
define([
	'$', 
	'mixture', 
	'mediator', 
	'mixin/view', 
	'view/swatch', 
	'mixin/toggle',
	'model/color',
	'vendor/event-emitter'
], 

function($, mix, Mediator, View, swatchView, Toggle, Color, EventEmitter){
	
	var id = -1;
	
	/** 
	 * Swatch - constructs a Swatch object for getting, storing, and displaying colors
	 * @class
	 * @param {Object} options implements color, draggable, View ( renderTarget, renderMethod )
	 * @param {Color} options.color
	 * @param {boolean} options.draggable
	 * @param {string} options.renderTarget
	 * @param {string} options.renderMethod
	 * 
	 * @mixes View
	 * @mixes Toggle
	 * @mixes EventEmitter
	 */
	var Swatch = function(options){
		
		var id = ++id;
		var currentColor = options.color;
		var draggable = options.draggable;
		var $renderTarget = $(options.renderTarget);
		
		mix( View, Toggle, EventEmitter ).using({
			view: swatchView,
			renderMethod: options.renderMethod,
			renderTarget: options.renderTarget,
			activeClass: 'controls-active',
			toggleElement: options.renderTarget
		}).into( this );
		
		/**
		 * @memberof Swatch#
		**/
		this.setColor = function (color){
			currentColor = color;
			currentColor.draggable = draggable;
			currentColor.id = id;
			this.updateView(currentColor);
		}

		/**
		 * @memberof Swatch#
		**/
		this.getColor = function () {
			return currentColor;
		}

		// Event handlers 

		var toggleControls = function (event) {
			event.preventDefault();
			event.stopPropagation();

			this.toggle();

		}.bind(this);

		var controlChange = function (event) {
			event.preventDefault();
			event.stopPropagation();
			
			var updatedColor = getColorFromSliderEvent(event);

			this.setColor( updatedColor );
			this.emitEvent.call(this, 'colorChange', [updatedColor]);

		}.bind(this);

		var updatePreview = function (event) {
			var updatedColor = getColorFromSliderEvent(event);
			
			this.getView().find('.preview')[0].style.backgroundColor = 'rgb(' + currentColor.rgb.join(', ') + ')';

		}.bind(this);

		function getColorFromSliderEvent(event) {
			var sliderType = event.target.className;
			var val = parseInt( event.target.value );
			var updatedColor = new Color( currentColor.rgb );

			switch (sliderType) {
				case 'red' : 
					updatedColor.modify('rgb', 0, val);
					break;
				case 'green' : 
					updatedColor.modify('rgb', 1, val);
					break;
				case 'blue' : 
					updatedColor.modify('rgb', 2, val);
					break;
				case 'lightness' : 
					updatedColor.modify('hsl', 2, val);
					break;
			}
			return updatedColor;
		}

		function init() {
			currentColor.id = id;
			currentColor.draggable = draggable;

			this.renderView(currentColor);

			$renderTarget.on('click touchstart', '.controls', toggleControls);
			$renderTarget.on('change', 'input[type=range]', controlChange);
			$renderTarget.on('input', 'input[type=range]', updatePreview);
		}
		
		init.call(this);
	}
	
	return Swatch;
});

