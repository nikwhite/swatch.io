# swatch.io

## API 

To get started running the swatch API, you must have pip installed at the very least. I also recommend using virtualenv and virtualenvwrapper so that you don't install python packages to system python.

Once you have pip working, do:

`pip install -r requirements.txt`

After that, you should be able to:

`python swatch.py`

If you'd like to run this in the background without occupying a terminal, do:

`nohup python swatch.py &`

Flask reloads the API code as you change it, so feel free to modify swatch.py and see your changes immediately.

## Front end

To build the front end code `node` must be installed. Once installed, run:

`npm install`

This will install `grunt` and required build dependencies.

Once installed, run `grunt` to build the templates and bundle the code.
